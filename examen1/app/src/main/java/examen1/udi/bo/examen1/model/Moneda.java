package examen1.udi.bo.examen1.model;

import examen1.udi.bo.examen1.exceptions.NegativeAmountException;

public class Moneda {

    private int tipoCambio;

    public double ConvertirBolivianoADolares(float bolivianos) throws NegativeAmountException {

        if (bolivianos < 0 || tipoCambio < 0) {
            throw new NegativeAmountException();
        }

        return bolivianos / tipoCambio;
    }

    public void setTipoCambio(int tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public int getTipoCambio() {
        return tipoCambio;
    }
}
