package examen1.udi.bo.examen1.exceptions;

public class NegativeAmountException extends Exception {

    public NegativeAmountException() {
        super("El numero debe ser positivo.");
    }
}
