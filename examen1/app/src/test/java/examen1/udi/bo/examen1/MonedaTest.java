package examen1.udi.bo.examen1;

import org.junit.Assert;
import org.junit.Test;

import examen1.udi.bo.examen1.model.Moneda;
import examen1.udi.bo.examen1.exceptions.*;

public class MonedaTest {

    @Test
    public void ConvertirBolivianosADolaresTest() throws NegativeAmountException {

        Moneda moneda = new Moneda();

        moneda.setTipoCambio(7);

        float bolivianos = 49;

        double result = moneda.ConvertirBolivianoADolares(bolivianos);
        Assert.assertEquals(7, result, 0.002);
    }

    @Test(expected = NegativeAmountException.class)
    public void ConvertBolivianosADolaresValidacionPositiva() throws NegativeAmountException {

        Moneda moneda = new Moneda();

        float bolivianos = -49;

        double result = moneda.ConvertirBolivianoADolares(bolivianos);
    }

}
